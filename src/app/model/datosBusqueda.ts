export class DatosBusqueda {
    /**
     * @type {number} id Unique numeric identifier.
     */
    id: number;
    /**
     * @type {string}
     */
    texto: String;
    /**
     * @type {string}
     */
    buscar1: String;
    /**
     * @type {number}
     */
    year: number;
  }
import { Routes } from '@angular/router';
import { BuscadorComponent } from '../../buscador/components/buscador.component';



export const BuscadorRoute: Routes = [
    {
        path: '',
        component: BuscadorComponent
    }
]
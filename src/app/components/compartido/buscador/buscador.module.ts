import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BuscadorRoute } from './buscador.route';
import {BuscadorComponent} from './../buscador/components/buscador.component';


@NgModule({
    declarations: [
        BuscadorComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(BuscadorRoute)
    ]
})
export class BuscadorModule{}
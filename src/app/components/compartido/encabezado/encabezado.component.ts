import { Component, ChangeDetectorRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { DatosBusquedaModel } from 'src/app/interfaces/busqueda';
import { BuscarService } from 'src/app/services/buscar.service';
import { ThemeService } from 'src/app/theme/theme.service';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.scss']
})

export class EncabezadoComponent implements OnInit {
  menus:any= [];
  subMenus:any= [];
  menuGeneral:any= [];
  primerMenu:any;
  segundoMenu:any = "";
  public seccionEncabezado: any;
  public ruta: string;
  public ruta_split: any;

  public esDispositivoMovil1: any;

  flag: boolean = true;
  menuLogo: boolean = true;
  menuList: boolean = true;
  menuBase: boolean = false;
  abrirMenu: boolean = false;
  cerrarMenu: boolean = false;
  categoriaW: boolean = false;
  subcategoriaW: boolean = false;
  uxEnlace: boolean = false;
  uiEnlace: boolean = false;
  desarrolloEnlace: boolean = false;
  investigacionUsuario: boolean = false;
  arqInformacion: boolean = false;
  disenoInterfaz: boolean = false;
  metricas: boolean = false;
  optimizacion: boolean = false;
  compWeb:boolean = false;
  recursosDiseno:boolean = false;
  lineamientosBackend:boolean = false;
  docFrontend:boolean = false;
  lineamientosAndroid:boolean = false;
  docChatbot:boolean = false;
  android: boolean = false;
  mostrarTextoBuscado: boolean = false;
  mostrarPalabrasFrecuentes: boolean = true;
  concordancias: DatosBusquedaModel[];
  textobuscado: string;
  showLogo: boolean = true;

  filter:Array<DatosBusquedaModel> = new Array<DatosBusquedaModel>();

  /* Borrar lo del buscador */
  searchValue: string = '';
  clearSearch() {
    this.searchValue = null;
  }

  /*Borrar con enter */
  onKeydown(valor:string, event ) {
    if (event.key === "Enter") {
      window.open('buscador/'+valor);
      this.searchValue = null;
    }
  }

  constructor(private _globalService: GlobalService, private buscarService: BuscarService, private router: Router, private cdRef:ChangeDetectorRef, private themeService: ThemeService) { }

  ngOnInit() {
    window.onscroll = () => {
      if (window.pageYOffset > 0 ) {
        document.querySelector('header').style.top = '-50px';
      } else {
        document.querySelector('header').style.top = '0';
      }

      this._globalService.wsMenuLateral = window.pageYOffset;
    };

    this.esDispositivoMovil1 = window.innerWidth;

    this.buscarService.getBuscar().subscribe(
      (buscar: DatosBusquedaModel[]) => {
        for (let i = 0; i < buscar.length; i++) {
          this.concordancias = buscar;
        }
      });
  }

  onKey(value: string) {
    if (value.length >= 1) {
      this.mostrarTextoBuscado = true;
    }
    else {
      this.mostrarTextoBuscado = false;
    }
  }

  ngAfterViewChecked(){
    this.seccionEncabezado = this.router.url.split("/")[1];
     this.cdRef.detectChanges();
  }

  toggleTheme() {
    if (this.themeService.isDarkTheme()) {
      this.themeService.setLightTheme();
    } else {
      this.themeService.setDarkTheme();
    }
  }

}

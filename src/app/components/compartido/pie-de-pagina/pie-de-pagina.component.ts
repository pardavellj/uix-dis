import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie-de-pagina',
  templateUrl: './pie-de-pagina.component.html',
  styleUrls: ['./pie-de-pagina.component.scss']
})
export class PieDePaginaComponent implements OnInit {
  menus:any= [];
  subMenus:any= [];
  menuGeneral:any= [];
  primerMenu:any;
  segundoMenu:any = "";
  public seccionEncabezado: any;
  public ruta: string;
  public ruta_split: any;

    public esDispositivoMovil1: any;

  flag: boolean = true;
  menuLogo: boolean = true;
  menuList: boolean = true;
  menuBase: boolean = false;
  abrirMenu: boolean = false;
  cerrarMenu: boolean = false;
  categoriaW: boolean = false;
  definifcionProyecto: boolean = false;
  identidadInstitucional: boolean = false;
  proyectosWeb: boolean = false;
  proyectosAndroid: boolean = false;
  comenzarProyecto: boolean = false; 
  usuarioNecesidades: boolean = false; 
  plantillasInvestigacion: boolean = false; 
  editarPlantillas: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import { BuscadorComponent } from './components/buscador.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BuscadorRoute } from './buscador.route';
import { BuscarPipe } from 'src/app/pipes/buscar.pipe';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
    declarations: [
        
        BuscadorComponent,
        BuscarPipe,
       
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(BuscadorRoute),
        NgxPaginationModule
    ]
})
export class BuscadorModule{}
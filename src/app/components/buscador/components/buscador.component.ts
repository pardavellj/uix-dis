import { Component, OnInit } from '@angular/core';
import { BuscarService } from 'src/app/services/buscar.service';
import { ActivatedRoute } from '@angular/router';
import { DatosBusquedaModel } from 'src/app/interfaces/busqueda';
import { PaginationInstance } from 'ngx-pagination';


@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit {
  pageActual = 1;
  concordancias: DatosBusquedaModel[];
  textobuscado: string;
  buscar: string;
  // startIndex = 0;
  // endIndex = 3;
  // length = 3;

  constructor(private buscarService: BuscarService, private router: ActivatedRoute) { }

buscador:  PaginationInstance = {
  itemsPerPage: 10,
  currentPage: 1,
  id: "pagin",
};

  ngOnInit() {

    this.buscarService.getBuscar().subscribe(
      (buscar: DatosBusquedaModel[]) => {
        for (let i = 0; i < buscar.length; i++) {
        this.concordancias = buscar;
        }
      });
    // Para obtener el parametro de la url
    this.router.params.subscribe(params => {
      this.textobuscado = params.textobuscado;
      this.buscar = this.textobuscado;
    });
  }
  getArrayFromNumber(length) {
  return new  Array (length = 3);
  }
  // updateIndex(pageIndex) {
  //   this.startIndex = pageIndex * ;
  //   this.endIndex = this.startIndex + 3;
  // }
}

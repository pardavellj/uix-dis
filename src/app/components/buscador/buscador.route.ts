import { Routes } from '@angular/router';
import { BuscadorComponent } from './components/buscador.component';


export const BuscadorRoute: Routes = [
    {
        path: '',
        component: BuscadorComponent
    }
]
import { Routes } from '@angular/router';
import { ComponentesUXComponent } from './components/componentes-ux.component';


export const ComponentesUXRoute: Routes = [
    {
        path: '',
        component: ComponentesUXComponent
    }
]
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-estructura',
  templateUrl: './estructura.component.html',
  styleUrls: ['./estructura.component.scss']
})
export class EstructuraComponent implements OnInit {

  @Input() seccion: any;

  constructor() { }

  ngOnInit() {
  }

}

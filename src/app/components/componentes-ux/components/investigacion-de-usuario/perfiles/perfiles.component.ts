import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.scss']
})
export class PerfilesComponent implements OnInit {

  @Input() seccion: any;

  constructor() { }

  ngOnInit() {
  }

}

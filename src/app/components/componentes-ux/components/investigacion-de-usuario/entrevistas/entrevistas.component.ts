import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-entrevistas',
  templateUrl: './entrevistas.component.html',
  styleUrls: ['./entrevistas.component.scss']
})
export class EntrevistasComponent implements OnInit {

  @Input() seccion: any;

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-prototipado',
  templateUrl: './prototipado.component.html',
  styleUrls: ['./prototipado.component.scss']
})
export class PrototipadoComponent implements OnInit {
  @Input () seccion: any;

  constructor() { }

  ngOnInit() {
  }

}

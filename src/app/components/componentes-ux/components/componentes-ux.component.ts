import { Component, ChangeDetectorRef, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';


@Component({
  selector: 'app-componentes-ux',
  templateUrl: './componentes-ux.component.html',
  styleUrls: ['./componentes-ux.component.scss']
})
export class ComponentesUXComponent implements OnInit {

  public ruta: string;
  public ruta_split: any;
  public general: any;
  public tipo: any;
  public seccion: any;
  public seccionEncabezado: any;
  public ruta_recursos: any;
  public unidadActiva: any;
  public informacion: any;
  public abierto: boolean;

  @Input() componente: any = "";

  constructor(private _globalService: GlobalService,private router: Router, private _activatedRoute: ActivatedRoute, private cd: ChangeDetectorRef) {
    this.ruta = this.router.url;
    this.ruta_split = this.ruta.split("/");
    this.ruta_recursos = this.ruta_split[0];
  }

  descripcion: any = {
    entrevistas: 'Utiliza el tipo de entrevista adecuado para conocer a fondo las necesidades de tus usuarios.',
    perfiles: 'Aprende a crear perfiles y mapear la experiencia de usuario a partir de los datos recabados en tus investigaciones.',
    estructura: 'Conoce y aplica las técnicas adecuadas para definir un sistema de navegación claro y eficiente, que brinde una excelente experiencia de usuario.',
    organización: 'Conoce cómo lograr una óptima organización de la información del sitio.',
    pruebas: 'Comprueba la eficiencia, eficacia y satisfacción de tu proyecto a través de estas técnicas de investigación.',
    'métricas': 'Analiza los resultados de tus investigaciones e implementa mejoras en los puntos débiles de tu proyecto para fortalecerlo.',
    'experiencia de Usuario - UX': 'Genera nuevas emociones en tus usuarios gracias al diseño de productos enfocados especialmente en sus necesidades, de fácil uso e interacción.',
    prototipado: 'Realiza prototipos en poco tiempo y a bajo costo guiándote con las técnicas que aquí se ofrecen.',
    storyboard: 'Presenta tu proyecto y comunica tus ideas a través de esta herramienta de narrativa visual.',
    'calidad de Interfaz': 'Verifica si tu propuesta gráfica cumple con los principios de diseño.',
    'protocolo Open Graph': 'Comparte de manera enriquecida tu sitio web en redes sociales.',
    'buenas Prácticas: URL': 'Conoce los principios para crear el URL adecuado.',
    placeholder: 'Proporciona a los usuarios una guía de llenado óptima.',
    favicon: 'Ayuda a los usuarios a recordar y reconocer tu proyecto digital entre las pestañas del navegador y brinda una experiencia de búsqueda efectiva.'
  };


  ngOnInit() {
    this.getParametersURL();
    this.abierto = false;
  }

  abrirMenuLateral() {
    if (this.abierto == true) {
      this.abierto = false;
    } else {
      this.abierto = true;
    }
  }

  getParametersURL() {
    this._activatedRoute.params.subscribe((params) => {
      this.general = params.general;
      this.tipo = params.tipo;
      this.seccion = params.seccion;
      this.seccionEncabezado = this.seccion;
      this.cd.detectChanges();
      if (this.tipo === 'organizacion') {
        this.tipo = 'organización';
      }
      if (this.tipo === 'experiencia') {
        this.tipo = 'experiencia de Usuario - UX';
      }
      if (this.tipo === 'calidad-de-interfaz') {
        this.tipo = 'calidad de Interfaz';
      }
      if (this.tipo === 'metricas') {
        this.tipo = 'métricas';
      }
      if (this.tipo === 'open-graph') {
        this.tipo = 'protocolo Open Graph'
      }
      if (this.tipo === 'buenas-practicas'){
        this.tipo = 'buenas Prácticas: URL'
      }
      this.informacion = this.descripcion[this.tipo];
    });
  }
}

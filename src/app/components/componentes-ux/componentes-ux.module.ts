import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentesUXRoute } from './componentes-ux.routes';
import { EntrevistasComponent } from './components/investigacion-de-usuario/entrevistas/entrevistas.component';
import { PerfilesComponent } from './components/investigacion-de-usuario/perfiles/perfiles.component';
import { EstructuraComponent } from './components/arquitectura-de-informacion/estructura/estructura.component';
import { OrganizacionComponent } from './components/arquitectura-de-informacion/organizacion/organizacion.component';
import { MetricasComponent } from './components/mejora-continua/metricas/metricas.component';
import { PruebasComponent } from './components/mejora-continua/pruebas/pruebas.component';
import { ExperienciaUsuarioComponent } from './components/experiencia-usuario/experiencia-usuario.component';
import { ComponentesUXComponent } from './components/componentes-ux.component';
import { DisenoDeInterfazComponent } from './components/diseno-de-interfaz/diseno-de-interfaz.component';
import { PrototipadoComponent } from './components/diseno-de-interfaz/prototipado/prototipado.component';
import { EscenarioComponent } from './components/diseno-de-interfaz/escenario/escenario.component';
import { CalidadDeInterfazComponent } from './components/diseno-de-interfaz/calidad-de-interfaz/calidad-de-interfaz.component';
import { OpenGraphComponent } from './components/optimizacion/open-graph/open-graph.component';
import { BuenasPracticasUrlComponent } from './components/optimizacion/buenas-practicas-url/buenas-practicas-url.component';
import { from } from 'rxjs';
import { PlaceholderComponent } from './components/optimizacion/placeholder/placeholder.component';
import { FaviconComponent } from './components/optimizacion/favicon/favicon.component';


@NgModule({
    declarations: [
        EntrevistasComponent,
        PerfilesComponent,
        EstructuraComponent,
        OrganizacionComponent,
        PruebasComponent,
        ExperienciaUsuarioComponent,
        ComponentesUXComponent,
        DisenoDeInterfazComponent,
        PrototipadoComponent,
        EscenarioComponent,
        CalidadDeInterfazComponent,
        OpenGraphComponent,
        BuenasPracticasUrlComponent,
        MetricasComponent,
        PlaceholderComponent,
        FaviconComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ComponentesUXRoute)
    ]
})
export class ComponentesUXModule { }

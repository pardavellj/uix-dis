import { Routes } from '@angular/router';
import { InicioComponent } from './components/inicio.component';


export const InicioRoute: Routes = [
    {
        path: '',
        component: InicioComponent
    }
]
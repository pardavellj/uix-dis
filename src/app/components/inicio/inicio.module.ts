import { NgModule } from '@angular/core';
import { InicioComponent } from './components/inicio.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InicioRoute } from './inicio.route';


@NgModule({
    declarations: [
        InicioComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(InicioRoute)
    ]
})
export class InicioModule{}
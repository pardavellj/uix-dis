import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-capas',
  templateUrl: './capas.component.html',
  styleUrls: ['./capas.component.scss']
})
export class CapasComponent implements OnInit {

  @Input() seccion: any;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

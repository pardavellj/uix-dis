import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-arquetipos',
  templateUrl: './arquetipos.component.html',
  styleUrls: ['./arquetipos.component.scss']
})
export class ArquetiposComponent implements OnInit {

  @Input() seccion: any;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

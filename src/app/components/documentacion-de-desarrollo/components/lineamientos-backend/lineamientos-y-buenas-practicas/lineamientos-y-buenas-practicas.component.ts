import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-lineamientos-y-buenas-practicas',
  templateUrl: './lineamientos-y-buenas-practicas.component.html',
  styleUrls: ['./lineamientos-y-buenas-practicas.component.scss']
})
export class LineamientosYBuenasPracticasComponent implements OnInit {


  @Input() seccion:any;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

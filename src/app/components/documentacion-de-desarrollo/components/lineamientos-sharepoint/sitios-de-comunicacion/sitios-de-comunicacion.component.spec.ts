import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitiosDeComunicacionComponent } from './sitios-de-comunicacion.component';

describe('SitiosDeComunicacionComponent', () => {
  let component: SitiosDeComunicacionComponent;
  let fixture: ComponentFixture<SitiosDeComunicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitiosDeComunicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitiosDeComunicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

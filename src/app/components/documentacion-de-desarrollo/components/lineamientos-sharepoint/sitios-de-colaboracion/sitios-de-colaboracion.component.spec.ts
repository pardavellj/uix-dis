import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitiosDeColaboracionComponent } from './sitios-de-colaboracion.component';

describe('SitiosDeColaboracionComponent', () => {
  let component: SitiosDeColaboracionComponent;
  let fixture: ComponentFixture<SitiosDeColaboracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitiosDeColaboracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitiosDeColaboracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

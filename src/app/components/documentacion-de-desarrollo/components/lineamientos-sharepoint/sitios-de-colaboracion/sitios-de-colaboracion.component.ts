import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-sitios-de-colaboracion',
  templateUrl: './sitios-de-colaboracion.component.html',
  styleUrls: ['./sitios-de-colaboracion.component.sass']
})
export class SitiosDeColaboracionComponent implements OnInit {
  @Input() seccion: any;
  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}


import { Component, OnInit , Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.sass']
})
export class SolicitudComponent implements OnInit {
  @Input() seccion: any;
  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

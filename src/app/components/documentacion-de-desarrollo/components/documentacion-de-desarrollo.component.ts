import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-documentacion-de-desarrollo',
  templateUrl: './documentacion-de-desarrollo.component.html',
  styleUrls: ['./documentacion-de-desarrollo.component.scss']
})
export class  DocumentacionDeDesarrolloComponent implements OnInit {

  public ruta: string;
  public ruta_split: any;
  public general: any;
  public tipo: any;
  public seccion: any;
  public ruta_recursos: any;
  public unidadActiva: any;
  public informacion: any;
  public abierto: boolean;

  @Input() componente: any;

  constructor(private _globalService: GlobalService, private router: Router, private _activatedRoute: ActivatedRoute) {
    this.ruta = this.router.url;
    this.ruta_split = this.ruta.split("/");
    this.ruta_recursos = this.ruta_split[0];
  }

  descripcion: any = {
    'Lineamientos generales y buenas practicas': 'Una estructura jerárquica de Project Object Model- POM INE',
    'Documentación de Desarrollo': 'Conoce las herramientas necesarias y los documentos de buenas prácticas para llevar a cabo tu desarrollo. Aquí encontrarás cómo implementar códigos e instalar un plugin.',
    arquetipos: 'Un Arquetipo es una plantilla de un proyecto Maven. Se define como un patrón o modelo original a partir del cual se hacen todas las demás cosas del mismo tipo.',
    capas: 'Al combinar las distintas capas se crearán los arquetipos que requieras.',
    scaffolding: 'Crea un proyecto utilizando el modelo vista-controlador para integrar el desarrollo backend de acuerdo con lineamientos del INE.',
    'Estructura en un proyecto Android': 'Conoce los elementos necesarios para crear una aplicación nativa.',
    'Configuración general': 'Descubre cómo implementar las configuraciones generales para los componentes Android.',
    'Lineamientos y buenas practicas Android': 'Conoce los lineamientos y buenas prácticas en el desarrollo de soluciones de software generadas dentro y para el Instituto Nacional Electoral.',
    'Arquitectura móvil': 'Estas vistas nos permitirán analizar el problema y describir el sistema desde el punto de vista de distintos interesados, como lo son los usuarios finales, los desarrolladores y/o jefes de proyecto.',
    'Capas MVVM':'Este modelo permite a los desarrolladores móvil utilizar el concepto de enlace de datos con un código reducido.','Maqueta tipo':'Personaliza tu aplicación con este prototipo base.',
     chatbot:'Conoce cómo diseñar un chatbot con buenas prácticas, desde la conversación hasta su desarrollo.',
     voicebot:'Crea un voicebot mediante buenas prácticas de diseño y de desarrollo.',
     'solicitud para crear un sitio web':'Conoce las recomendaciones para que tu solicitud sea más sencilla y que el sitio cumpla con lo requerido.','sitios de Comunicación':'Descubre cómo crear sitios SharePoint de comunicación para difundir información a una mayor audiencia como noticias, informes y proyectos.','sitios de Colaboración':'Crea sitios para compartir información y trabajar con un grupo de usuarios.'

     
  };

  ngOnInit() {
    this.getParametersURL();
    this.abierto = false;
  }

  abrirMenuLateral() {
    if (this.abierto == true) {
      this.abierto = false;
    } else {
      this.abierto = true;
    }
  }
  getParametersURL() {
    this._activatedRoute.params.subscribe((params) => {
      this.general = params.general;
      this.tipo = params.tipo;
      this.seccion = params.seccion;
      if (this.tipo === 'buenas-practicas') {
        this.tipo = 'Lineamientos generales y buenas practicas';
      }
      if (this.tipo === 'desarrollo') {
        this.tipo = 'Documentación de Desarrollo';
      }
      if (this.tipo === 'estructura-en-un-proyecto') {
        this.tipo = 'Estructura en un proyecto Android';
      }
      if (this.tipo === 'configuracion-general') {
        this.tipo = 'Configuración general';
      }
      if (this.tipo === 'lineamientos-y-buenas-practicas-android') {
        this.tipo = 'Lineamientos y buenas practicas Android';
      }
      if (this.tipo === 'arquitectura-movil') {
        this.tipo = 'Arquitectura móvil';
      }
      if (this.tipo === 'capas-movil') {
        this.tipo = 'Capas MVVM';
     
      if (this.tipo === 'interfaz') {
        this.tipo = 'interfaz de Usuario - UI';
      }
       }if (this.tipo === 'solicitud') {
        this.tipo = 'solicitud para crear un sitio web';
      }
      if (this.tipo === 'sitios-de-comunicacion') {
        this.tipo = 'sitios de Comunicación';
      }
      if (this.tipo === 'sitios-de-colaboracion') {
        this.tipo = 'sitios de Colaboración';
      }
      if (this.tipo === 'maqueta-tipo') {
        this.tipo = 'Maqueta tipo';
      }
      
      
      this.informacion = this.descripcion[this.tipo];
      this.informacion = this.descripcion[this.tipo];
    
    });
  }

}

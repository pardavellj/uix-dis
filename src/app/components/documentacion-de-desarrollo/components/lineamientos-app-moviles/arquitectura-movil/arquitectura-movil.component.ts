import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';


@Component({
  selector: 'app-arquitectura-movil',
  templateUrl: './arquitectura-movil.component.html',
  styleUrls: ['./arquitectura-movil.component.scss']
})
export class ArquitecturaMovilComponent implements OnInit {
  @Input() seccion: any;

    constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';

@Component({
  selector: 'app-estructura-en-un-proyecto',
  templateUrl: './estructura-en-un-proyecto.component.html',
  styleUrls: ['./estructura-en-un-proyecto.component.scss']
})
export class EstructuraEnUnProyectoComponent implements OnInit {

  @Input() seccion: any;

  constructor(private _copyToClipboard:CopyToClipboardService) { }

  ngOnInit() {
  }
  CopyToClipboard( idElemento ){
    this._copyToClipboard.CopyToClipboard( idElemento );
}
}

import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-lineamientos-y-buenas-practicas-android',
  templateUrl: './lineamientos-y-buenas-practicas-android.component.html',
  styleUrls: ['./lineamientos-y-buenas-practicas-android.component.scss']
})
export class LineamientosYBuenasPracticasAndroidComponent implements OnInit {

  @Input() seccion:any;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }
}

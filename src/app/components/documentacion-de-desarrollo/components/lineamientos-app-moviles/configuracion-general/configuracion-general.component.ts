import { Component, OnInit, Input } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';

@Component({
  selector: 'app-configuracion-general',
  templateUrl: './configuracion-general.component.html',
  styleUrls: ['./configuracion-general.component.scss']
})
export class ConfiguracionGeneralComponent implements OnInit {

  @Input() seccion: any;
  
  constructor(private _copyToClipboard:CopyToClipboardService) { }

  ngOnInit() {
  }
  CopyToClipboard( idElemento ){
    this._copyToClipboard.CopyToClipboard( idElemento );
}
}

import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-capas-movil',
  templateUrl: './capas-movil.component.html',
  styleUrls: ['./capas-movil.component.scss']
})
export class CapasMovilComponent implements OnInit {
  @Input() seccion: any;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

}

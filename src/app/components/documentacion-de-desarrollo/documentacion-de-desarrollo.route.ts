import { Routes } from '@angular/router';
import {DocumentacionDeDesarrolloComponent} from './components/documentacion-de-desarrollo.component';

export const DocumentacionDeDesarrolloRoute: Routes = [
    {
        path: '',
        component: DocumentacionDeDesarrolloComponent
    }
]
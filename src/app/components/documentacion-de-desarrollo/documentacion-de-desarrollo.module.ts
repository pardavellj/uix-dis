import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DocumentacionDeDesarrolloRoute } from './documentacion-de-desarrollo.route';
// tslint:disable-next-line: max-line-length
import {LineamientosYBuenasPracticasComponent } from './components/lineamientos-backend/lineamientos-y-buenas-practicas/lineamientos-y-buenas-practicas.component';
import { ArquetiposComponent } from './components/lineamientos-backend/arquetipos/arquetipos.component';
import { CapasComponent } from './components/lineamientos-backend/capas/capas.component';
import { AngularComponent } from './components/documentacion-frontend/angular/angular.component';
import { ConocimientosTsComponent } from './components/documentacion-frontend/conocimientos-ts/conocimientos-ts.component';
import { IdsDesarrolloComponent } from './components/documentacion-frontend/ids-desarrollo/ids-desarrollo.component';
import { DocumentacionEstandarComponent } from './components/documentacion-estandar/documentacion-estandar.component';
import { DocumentacionDeDesarrolloComponent } from './components/documentacion-de-desarrollo.component';
import { from } from 'rxjs';
import { ScaffoldingComponent } from './components/documentacion-frontend/scaffolding/scaffolding.component';
import { EstructuraEnUnProyectoComponent } from './components/lineamientos-app-moviles/estructura-en-un-proyecto/estructura-en-un-proyecto.component';
import { ConfiguracionGeneralComponent } from './components/lineamientos-app-moviles/configuracion-general/configuracion-general.component';
import { LineamientosYBuenasPracticasAndroidComponent } from './components/lineamientos-app-moviles/lineamientos-y-buenas-practicas-android/lineamientos-y-buenas-practicas-android.component';
import { ArquitecturaMovilComponent } from './components/lineamientos-app-moviles/arquitectura-movil/arquitectura-movil.component';
import { CapasMovilComponent } from './components/lineamientos-app-moviles/capas-movil/capas-movil.component';
import { ChatbotComponent } from './components/lineamientos-asistentes-virtuales/chatbot/chatbot.component';
import { MaquetaTipoComponent } from './components/lineamientos-app-moviles/maqueta-tipo/maqueta-tipo.component';
import { VoicebotComponent } from './components/lineamientos-asistentes-virtuales/voicebot/voicebot.component';
import { SolicitudComponent } from './components/lineamientos-sharepoint/solicitud/solicitud.component';
import { SitiosDeComunicacionComponent } from './components/lineamientos-sharepoint/sitios-de-comunicacion/sitios-de-comunicacion.component';
import { SitiosDeColaboracionComponent } from './components/lineamientos-sharepoint/sitios-de-colaboracion/sitios-de-colaboracion.component';


@NgModule({
    declarations: [
        LineamientosYBuenasPracticasComponent,
        ArquetiposComponent,
        CapasComponent,
        AngularComponent,
        ConocimientosTsComponent,
        IdsDesarrolloComponent,
        DocumentacionEstandarComponent,
        DocumentacionDeDesarrolloComponent,
        ScaffoldingComponent,
        EstructuraEnUnProyectoComponent,
        ConfiguracionGeneralComponent,
        LineamientosYBuenasPracticasAndroidComponent,
        ArquitecturaMovilComponent,
        CapasMovilComponent,
        ChatbotComponent,
        MaquetaTipoComponent,
        VoicebotComponent,
        SolicitudComponent,
        SitiosDeComunicacionComponent,
        SitiosDeColaboracionComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(DocumentacionDeDesarrolloRoute)
    ]
})
export class DocumentacionDeDesarrolloModule{ }
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tablas',
  templateUrl: './tablas.component.html',
  styleUrls: ['./tablas.component.scss']
})
export class TablasComponent implements OnInit {

  public data : any

  constructor() { }

  ngOnInit() {

    this.data = [
    {'name':'Dato1', 'email' :'Dato1', 'age' :'Dato1', 'city':'Dato1' },
    {'name':'Dato2', 'email' :'Dato2', 'age' :'Dato2', 'city':'Dato2' },
    {'name':'Dato3', 'email' :'Dato3', 'age' :'Dato3', 'city':'Dato3' },
    {'name':'Dato4', 'email' :'Dato4', 'age' :'Dato4', 'city':'Dato4' },
    {'name':'Dato5', 'email' :'Dato5', 'age' :'Dato5', 'city':'Dato5' },
    {'name':'Dato6', 'email' :'Dato6', 'age' :'Dato6', 'city':'Dato6' },
    {'name':'Dato7', 'email' :'Dato7', 'age' :'Dato7', 'city':'Dato7' },
    {'name':'Dato8', 'email' :'Dato8', 'age' :'Dato8', 'city':'Dato8' },
    {'name':'Dato9', 'email' :'Dato9', 'age' :'Dato9', 'city':'Dato9' }
   ]

  }

}

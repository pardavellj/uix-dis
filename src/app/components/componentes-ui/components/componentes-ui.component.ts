import { Component, ChangeDetectorRef, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';


@Component({
  selector: 'app-componentes-ui',
  templateUrl: './componentes-ui.component.html',
  styleUrls: ['./componentes-ui.component.scss']
})
export class ComponentesUIComponent implements OnInit {

  public ruta: string;
  public ruta_split: any;
  public general: any;
  public tipo: any;
  public seccion: any;
  public ruta_recursos: any;
  public unidadActiva: any;
  public informacion: any;
  public abierto: boolean;

  @Input() componente: any = "";

  constructor(private _globalService: GlobalService,private router: Router, private _activatedRoute: ActivatedRoute, private cd: ChangeDetectorRef) {
    this.ruta = this.router.url;
    this.ruta_split = this.ruta.split("/");
    this.ruta_recursos = this.ruta_split[0];
  }

  descripcion: any = {
    button: 'Creados para la interacción, deben ser fácilmente identificables y responder a las acciones de los usuarios.',
    input: 'Permiten al usuario interactuar e ingresar contenidos y datos.',
    checkbox: 'Permite a los usuarios la selección de una o más opciones en un listado.',
    'radio button': 'Excluye información entre opciones, permitiendo al usuario realizar una sola selección. ',
    search: 'Permite consultar resultados de una selección de datos y buscar dentro de una fuente local estática.',
    dropdowns: 'Se utiliza principalmente en formularios, donde el usuario envía datos y elige una opción en una lista.',
    datepicker: 'Ayuda a seleccionar una fecha, identificar la hora exacta e incluso a seleccionar días relevantes.',
    toggle: 'Se utiliza como interruptor de dos estados posibles, comúnmente “encendido / apagado”.',
    notification: 'Sirven para notificar al usuario sobre un suceso o acción ocurrido.',
    tooltips: 'Muestra información adicional cuando un usuario se posiciona sobre un componente.',
    breadcrumb: 'Facilitan la usabilidad de las páginas y ayudan al posicionarlas cuando se incorporan adecuadamente.',
    pagination: 'Ordena la información por páginas de resultados para así ubicar al usuario.',
    'nav tabs': 'Ayudan a los usuarios para cambiar de contenidos sin hacerlo necesariamente de página.',
    upload: 'Agrega uno o varios archivos a fin de guardar o enviar información complementaria.',
    cards: 'Destacan cierto apartado o agrupan elementos para facilitar el escaneo de información.',
    modal: 'Permiten enfocar la atención del usuario en un diálogo que detona un nuevo flujo.',
    progressbar: 'Muestra de forma gráfica el estado de avance de una tarea o proceso.',
    accordion: 'Permite mostrar la información en bloques, desplegándose en función de la interacción con el usuario.',
    spinner: 'Evita el estrés o frustración en el usuario cuando el sistema ejecuta algún proceso.',
    picklist: 'Permite que el usuario elija varios elementos desde un doble listado, haciendo una selección múltiple o individual.',
    color: 'Aplica la paleta de color del INE y sus variantes en tus proyectos.',
    'Ilustraciones': 'Conoce como utilizar ilustraciones consistentes con la línea de diseño gráfico del INE.',
    'identificadores gráficos': 'Conoce las especificaciones de un identificador gráfico.',
    Iconografía: 'Descarga la iconografía del Instituto para mantener una consistencia visual.',
    tipografía: 'Utiliza adecuadamente la tipografía oficial del Instituto.',
    formulario: 'Utiliza este formulario como base para crear los tuyos.',
    toast: 'Notifica al usuario sobre temas no críticos sin interrumpir su navegación',
    'data table': 'Se utiliza para mostrar información ordenada y estructurada que ayuda a los usuarios a visualizar el contenido de forma clara.',
    'interfaz de Usuario - UI': 'Desarrolla sitios de interacción sencilla y útil, con componentes funcionales pensados para los usuarios, capaces de adaptarse a cualquier dispositivo.',
    // componentes-android
    'app bar: top': 'Muestra información y acciones relacionadas con la pantalla actual.',
    'bottom navigation': 'Permite el movimiento entre destinos principales de la aplicación.',
    buttons: 'Permiten al usuario tomar acciones y decisiones con un toque.',
    'buttons: floating action button': 'Muestra la acción principal de la pantalla.',
    card: 'Son superficies que agrupan contenido y acciones sobre un solo tema.',
    chips: 'Son elementos compactos que representan una entrada de datos, atributos o acciones.',
    dialogs: 'Informan a los usuarios sobre una tarea y pueden contener información crítica, requerir decisiones o involucrar múltiples tareas.',
    dividers: 'Son líneas delgadas que agrupan el contenido en listas y composiciones.',
    menus: 'Muestran una lista de opciones en superficies temporales.',
    'splash screen': 'Es la primera experiencia del usuario en tu aplicación.',
    tabs: 'Organizan el contenido en diferentes pantallas, conjuntos de datos y otras interacciones.',
    
  };


  ngOnInit() {
    this.getParametersURL();
    this.abierto = false;

  }

  abrirMenuLateral() {
    if (this.abierto == true) {
      this.abierto = false;
    } else {
      this.abierto = true;
    }
  }

  getParametersURL() {
    this._activatedRoute.params.subscribe((params) => {
      this.general = params.general;
      this.tipo = params.tipo;
      this.seccion = params.seccion;
      this.cd.detectChanges();
      if (this.tipo === 'elementos-de-diseno') {
        this.tipo = this.seccion;
      }
      if (this.seccion === 'iconografia') {
        this.tipo = 'Iconografía';
      }
      if (this.seccion === 'formulario') {
        this.tipo = 'formulario';
        this.seccion = 'estilo';
      }
      if (this.seccion === 'identificadores-graficos') {
        this.tipo = 'identificadores gráficos';
      }
      if (this.seccion === 'ilustraciones') {
        this.tipo = 'Ilustraciones';
      }
      if (this.seccion === 'tipografia') {
        this.tipo = 'tipografía';
      }
      if (this.tipo === 'pick-list') {
        this.tipo = 'picklist';
      }
      if (this.tipo === 'date-picker') {
        this.tipo = 'datepicker';
      }
      if (this.tipo === 'radio') {
        this.tipo = 'radio button';
      }
      if (this.tipo === 'data-table') {
        this.tipo = 'data table';
      }
      if (this.tipo === 'navtabs') {
        this.tipo = 'nav tabs';
      }
      if (this.tipo === 'interfaz') {
        this.tipo = 'interfaz de Usuario - UI';
      }
      if (this.tipo === 'app-bar-top'){
        this.tipo = 'app bar: top'
      }
      if(this.tipo === 'bottom-navigation'){
        this.tipo = 'bottom navigation'
      }
      if(this.tipo === 'buttons'){
        this.tipo ='buttons'
      }
      if(this.tipo === 'buttons-floating-action-button'){
        this.tipo ='buttons: floating action button'
      }
      if(this.tipo === 'card'){
        this.tipo = 'card'
      }
      if(this.tipo === 'dialogs'){
        this.tipo = 'dialogs'
      }
      if(this.tipo === 'dividers'){
        this.tipo = 'dividers'
      }
      if(this.tipo === 'menus'){
        this.tipo = 'menus'
      }
      if(this.tipo === 'splash-screen'){
        this.tipo = 'splash screen'
      }
      if(this.tipo === 'tabs'){
        this.tipo = 'tabs'
      }
      if(this.tipo === 'chips'){
        this.tipo = 'chips'
      }
      if(this.tipo === 'text-fields'){
        this.tipo = 'text fields'
      }
    
      this.informacion = this.descripcion[this.tipo];
      this.informacion = this.descripcion[this.tipo];
    });
  }

}

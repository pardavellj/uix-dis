import { Component, OnInit, Input } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  @Input() seccion:any;
  fragment:any;
  constructor(private _copyToClipboard:CopyToClipboardService) { }

  ngOnInit() {
  }
  ngAfterViewChecked() {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
 }
 CopyToClipboard( idElemento ){
     this._copyToClipboard.CopyToClipboard( idElemento );
 }
}

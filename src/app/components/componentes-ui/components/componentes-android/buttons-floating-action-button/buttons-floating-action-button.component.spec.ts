import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsFloatingActionButtonComponent } from './buttons-floating-action-button.component';

describe('ButtonsFloatingActionButtonComponent', () => {
  let component: ButtonsFloatingActionButtonComponent;
  let fixture: ComponentFixture<ButtonsFloatingActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonsFloatingActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsFloatingActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

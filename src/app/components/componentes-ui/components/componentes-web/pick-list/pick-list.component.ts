import { Component, OnInit, Input } from '@angular/core';
import { DualListComponent } from 'angular-dual-listbox';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';

@Component({
  selector: 'app-pick-list',
  templateUrl: './pick-list.component.html',
  styleUrls: ['./pick-list.component.scss']
})
export class PickListComponent implements OnInit {

  @Input() seccion: any;
  source = ['Opción 1', 'Opción 2', 'Opción 3', 'Opción 4', 'Opción 5', 'Opción 6', 'Opción 7'];
  target = [];
  format = {
    add: 'Agregar', remove: 'Regresar', all: 'Agregar todo', none: 'Ninguno',
    direction: DualListComponent.LTR, draggable: true
  };

  fragment: any;

  constructor(private _copyToClipboard: CopyToClipboardService) { }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  CopyToClipboard(idElemento) {
    this._copyToClipboard.CopyToClipboard(idElemento);
  }


}

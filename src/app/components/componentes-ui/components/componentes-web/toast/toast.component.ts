import { Component, OnInit, Input } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';



@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent implements OnInit {
  private _success = new Subject<string>();
  toastAutomatico = '';
  mostrarExito: boolean=false;
  mostrarAdvertencia: boolean=false;
  mostrarError: boolean=false;
  mostrarInformativo: boolean=false;
  mostrarAyuda: boolean=false;

 
  @Input() seccion: any;

  fragment: any;

  constructor(private _copyToClipboard: CopyToClipboardService,) {

  }
  ngOnInit(): void {
    this._success.subscribe(message => this.toastAutomatico = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.toastAutomatico = '');
  }

  public changeToastAutomatico() {
    this._success.next(`${new Date()} - Message successfully changed.`);
  }

  toastExito(valor:boolean){
    this.mostrarExito=valor;
  }

  toastAdvertencia(valor:boolean){
    this.mostrarAdvertencia=valor;
  }

  toastError(valor:boolean){
    this.mostrarError=valor;
  }

  toastInformativo(valor:boolean){
    this.mostrarInformativo=valor;
  }

  toastAyuda(valor:boolean){
    this.mostrarAyuda=valor;
  }

  ngAfterViewChecked() {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  CopyToClipboard(idElemento) {
    this._copyToClipboard.CopyToClipboard(idElemento);
  }

}

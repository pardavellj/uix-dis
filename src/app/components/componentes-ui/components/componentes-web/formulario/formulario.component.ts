
import { Component, ChangeDetectorRef, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CopyToClipboardService } from "src/app/services/copiaraportapapeles.service";


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
})


export class FormularioComponent implements OnInit {


  @Input() componente: any = '';
  forma: FormGroup;

  public ruta: string;
  public rutaSplit: any;
  public tipo: any;
  public seccion: any;
  public rutaRecursos: any;
  public unidadActiva: any;
  public informacion: any;
  public entidades: any;
  public municipios: any;
  public nombreEntidad: any;
  public habilitarSelect: boolean;
  public habiliarCheck: any;

  fragment: any;


  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;


  clave: Object = {
    clavecompleta: {
      clave1: "XAXAXA",
      clave2: "01010101",
      clave3: "X010"
    }
  };

  selecttipo: Object = {
    sexo: {
      hombre: "XAXAXA",
      mujer: "01010101"
    }
  };


  constructor(private router: Router, private _activatedRoute: ActivatedRoute, private cd: ChangeDetectorRef, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private _copyToClipboard: CopyToClipboardService) {
    this.forma = new FormGroup({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'primer-apellido': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'segundo-apellido': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'clavecompleta': new FormGroup({
        'clave1': new FormControl('', [Validators.required, Validators.minLength(6)]),
        'clave2': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{8}$")]),
        'clave3': new FormControl('', [Validators.required, Validators.pattern("^[A-Z0-9]{4}")])
      }),
      'ocr': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{13}")]),
      'sexo': new FormControl('', Validators.required),
      'entidad': new FormControl('', Validators.required),
      'municipio': new FormControl('', Validators.required),
      'fecha-de-nacimiento': new FormControl('', [Validators.required]),
      'lugar-de-nacimiento': new FormControl('', [Validators.required,]),
      'calle': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'numero-exterior': new FormControl('', [Validators.required, Validators.pattern("^(?=[0-9]*$)(?:.{1}|.{2}|.{3}|.{4})$")]),
      'numero-interior': new FormControl('', [Validators.required, Validators.pattern("^(?=[0-9]*$)(?:.{1}|.{2}|.{3}|.{4})$")]),
      'codigoPostal': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{5}$")]),
      'numero-telefonico': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{8}$")]),
      'correo': new FormControl('', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")])
    });


    //Select Entidades/Municipios
    this.habilitarSelect = true;
    this.forma.controls['municipio'].disable();
    this.entidades = ['Aguascalientes', 'Baja California', 'Baja California Sur', 'Campeche', 'Coahuila', 'Colima', 'Chiapas', 'Chihuahua', 'Ciudad de México', 'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'México', 'Michoacán', 'Morelos', 'Nayarit', 'Nuevo León', 'Oaxaca', 'Puebla', 'Querétaro', 'Quintana Roo', 'San Luis Potosí', 'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatán', 'Zacatecas'];
    this.municipios = {
      "Aguascalientes":
        [{ "nombre": "Aguascalientes" },
        { "nombre": "Asientos" },
        { "nombre": "Calvillo" },
        { "nombre": "Cosío" },
        { "nombre": "Jesús María" }],

      "Baja California":
        [{ "nombre": "Ensenada" },
        { "nombre": "Mexicali" },
        { "nombre": "Tecate" },
        { "nombre": "Tijuana" },
        { "nombre": "Playas de Rosarito" }],

      "Baja California Sur":
        [{ "nombre": "Comondú" },
        { "nombre": "Mulegé" },
        { "nombre": "La Paz" },
        { "nombre": "Los Cabos" },
        { "nombre": "Loreto" }],

      "Campeche":
        [{ "nombre": "Calkiní" },
        { "nombre": "Campeche" },
        { "nombre": "Carmen" },
        { "nombre": "Champotón" },
        { "nombre": "Hecelchakán" },
        { "nombre": "Hopelchén" },
        { "nombre": "Palizada" },
        { "nombre": "Tenabo" },
        { "nombre": "Escárcega" },
        { "nombre": "Calakmul" },
        { "nombre": "Candelaria" },
        { "nombre": "Seybaplaya" }]
    }

    this.ruta = this.router.url;
    this.rutaSplit = this.ruta.split("/");
    this.rutaRecursos = this.rutaSplit[0];
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }


  guardarCambios() {
  }

  ngOnInit() {
    this.getParametersURL();
  }

  obtenerEntidad() {
    this.nombreEntidad = this.forma.controls.entidad.value;
    if (this.nombreEntidad != "") {
      this.forma.controls['municipio'].enable();
    }
  }

  getParametersURL() {
    this._activatedRoute.params.subscribe((params) => {
      this.tipo = params.tipo;
      this.seccion = params.seccion;
      this.cd.detectChanges();
      if (this.seccion === 'formulario') {
        this.tipo = 'formulario';
        this.seccion = 'estilo';
      }
    });

  }

  ngAfterViewChecked() {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  CopyToClipboard(idElemento) {
    this._copyToClipboard.CopyToClipboard(idElemento);
  }



}
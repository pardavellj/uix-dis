import { Component, OnInit, Input, } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})


export class InputComponent implements OnInit {
  @Input() seccion: any;
  passwordType: string = 'password';

  fragment: any;
  constructor(private _copyToClipboard: CopyToClipboardService) {
  }

  ngOnInit() {
  }

  CopyToClipboard(idElemento) {
    this._copyToClipboard.CopyToClipboard(idElemento);
  }

  public togglePassword() {

    this.passwordType == "password" ? this.passwordType = "text" : this.passwordType = "password";

  }

}


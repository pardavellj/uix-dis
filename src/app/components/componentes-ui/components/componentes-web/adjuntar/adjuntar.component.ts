import { Component, OnInit, Input } from '@angular/core';
import { CopyToClipboardService } from 'src/app/services/copiaraportapapeles.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';


@Component({
  selector: 'app-adjuntar',
  templateUrl: './adjuntar.component.html',
  styleUrls: ['./adjuntar.component.scss']
})
export class AdjuntarComponent implements OnInit {

  @Input() seccion: any;
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  public fileOver(event) {
  }

  public fileLeave(event) {
  }

  fragment: any;

  constructor(private _copyToClipboard: CopyToClipboardService) {}

  ngOnInit() {
  }
  ngAfterViewChecked() {

    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  CopyToClipboard(idElemento) {
    this._copyToClipboard.CopyToClipboard(idElemento);
  }

}

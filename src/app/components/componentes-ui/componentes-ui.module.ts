import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ComponentesUIRoutes } from './componentes-ui.routes';
import { ComboComponent } from './components/combo/combo.component';
import { AppBarTopComponent } from './components/componentes-android/app-bar-top/app-bar-top.component';
import { BottomNavigationComponent } from './components/componentes-android/bottom-navigation/bottom-navigation.component';
import { ButtonsComponent } from './components/componentes-android/buttons/buttons.component';
import { CardsComponent } from './components/componentes-android/cards/cards.component';
import { ChipsComponent } from './components/componentes-android/chips/chips.component';
import { DialogsComponent } from './components/componentes-android/dialogs/dialogs.component';
import { DividersComponent } from './components/componentes-android/dividers/dividers.component';
import { MenusComponent } from './components/componentes-android/menus/menus.component';
import { SplashScreenComponent } from './components/componentes-android/splash-screen/splash-screen.component';
import { TabsComponent } from './components/componentes-android/tabs/tabs.component';
import { TextFieldsComponent } from './components/componentes-android/text-fields/text-fields.component';
import { ComponentesUIComponent } from './components/componentes-ui.component';
import { AcordionComponent } from './components/componentes-web/acordion/acordion.component';
import { AdjuntarComponent } from './components/componentes-web/adjuntar/adjuntar.component';
import { AlertasComponent } from './components/componentes-web/alertas/alertas.component';
import { BotonesComponent } from './components/componentes-web/botones/botones.component';
import { CalendarComponent } from './components/componentes-web/calendar/calendar.component';
import { CardComponent } from './components/componentes-web/card/card.component';
import { CheckboxComponent } from './components/componentes-web/checkbox/checkbox.component';
import { InputComponent } from './components/componentes-web/input/input.component';
import { MigajasComponent } from './components/componentes-web/migajas/migajas.component';
import { ModalComponent } from './components/componentes-web/modal/modal.component';
import { NavbarComponent } from './components/componentes-web/navbar/navbar.component';
import { PaginacionComponent } from './components/componentes-web/paginacion/paginacion.component';
import { PestanasComponent } from './components/componentes-web/pestanas/pestanas.component';
import { PickListComponent } from './components/componentes-web/pick-list/pick-list.component';
import { ProgressbarComponent } from './components/componentes-web/progressbar/progressbar.component';
import { RadioComponent } from './components/componentes-web/radio/radio.component';
import { SearchComponent } from './components/componentes-web/search/search.component';
import { SpinnerComponent } from './components/componentes-web/spinner/spinner.component';
import { ToggleComponent } from './components/componentes-web/toggle/toggle.component';
import { TooltipsComponent } from './components/componentes-web/tooltips/tooltips.component';
import { ColorComponent } from './components/elementos-de-diseno/color/color.component';
import { IconografiaComponent } from './components/elementos-de-diseno/iconografia/iconografia.component';
import { IdentificadoresGraficosComponent } from './components/elementos-de-diseno/identificadores-graficos/identificadores-graficos.component';
import { LineaDeDisenoGraficoComponent } from './components/elementos-de-diseno/ilustraciones/linea-de-diseno-grafico.component';
import { IlustracionesComponent } from './components/elementos-de-diseno/ilustraciones/ilustraciones.component';
import { TipografiaComponent } from './components/elementos-de-diseno/tipografia/tipografia.component';
import { InterfazUsuarioComponent } from './components/interfaz-usuario/interfaz-usuario.component';
import { PanelDeAcordeonComponent } from './components/panel-de-acordeon/panel-de-acordeon.component';
import { ProgresoComponent } from './components/progreso/progreso.component';
import { TablasComponent } from './components/tablas/tablas.component';
import { WizardComponent } from './components/wizard/wizard.component';
import { FormularioComponent } from './components/componentes-web/formulario/formulario.component';
import { ButtonsFloatingActionButtonComponent } from './components/componentes-android/buttons-floating-action-button/buttons-floating-action-button.component';
import { ToastComponent } from './components/componentes-web/toast/toast.component';
import { DataTableComponent } from './components/componentes-web/data-table/data-table.component';



@NgModule({
    declarations: [
        FormularioComponent,
        AcordionComponent,
        AdjuntarComponent,
        AlertasComponent,
        BotonesComponent,
        CalendarComponent,
        CardComponent,
        CheckboxComponent,
        InputComponent,
        MigajasComponent,
        ModalComponent,
        NavbarComponent,
        PaginacionComponent,
        PestanasComponent,
        PickListComponent,
        ProgressbarComponent,
        RadioComponent,
        SearchComponent,
        SpinnerComponent,
        ToggleComponent,
        TooltipsComponent,
        ColorComponent,
        IconografiaComponent,
        LineaDeDisenoGraficoComponent,
        IdentificadoresGraficosComponent,
        IlustracionesComponent,
        TipografiaComponent,
        InterfazUsuarioComponent,
        TablasComponent,
        ComboComponent,
        PanelDeAcordeonComponent,
        ProgresoComponent,
        WizardComponent,
        ComponentesUIComponent,
        AppBarTopComponent,
        BottomNavigationComponent,
        ButtonsComponent,
        CardsComponent,
        DialogsComponent,
        DividersComponent,
        MenusComponent,
        SplashScreenComponent,
        TabsComponent,
        TextFieldsComponent,
        ChipsComponent,
        ButtonsFloatingActionButtonComponent,
        ToastComponent,
        DataTableComponent
    ],
    imports: [
        CommonModule,
        NgxFileDropModule,
        NgbModule,
        ReactiveFormsModule,
        FormsModule,
        AngularDualListBoxModule,
        RouterModule.forChild(ComponentesUIRoutes)
    ]
})
export class ComponentesUIModule {}
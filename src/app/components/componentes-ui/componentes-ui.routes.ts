import { Routes } from '@angular/router';
import { ComponentesUIComponent } from './components/componentes-ui.component';


export const ComponentesUIRoutes: Routes = [
    {
        path: '',
        component: ComponentesUIComponent
    }
]
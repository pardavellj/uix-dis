import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Routes } from '@angular/router';

@Component({
  selector: 'app-pantallas-unicom',
  templateUrl: './pantallas-unicom.component.html',
  styleUrls: ['./pantallas-unicom.component.scss']
})
export class PantallasUnicomComponent implements OnInit {

  public ruta:string;
  public ruta_split:any;
  public tipo:any;
  public seccion:any;
  public ruta_recursos:any;
  public unidadActiva:any;
  public informacion:any;
  public abierto:boolean;

  @Input() componente: any;

  constructor(private router: Router, private _activatedRoute:ActivatedRoute) {
    this.ruta = this.router.url;
   // console.log(this.ruta);
    this.ruta_split=this.ruta.split("/");
    this.ruta_recursos= this.ruta_split[0];
    // console.log (this.ruta_recursos);
    console.log(this.ruta_split[3]);
   }

   descripcion:any = {
    unicom : 'bla bla bla'
    };

  ngOnInit() {
    this.getParametersURL();
      this.abierto = false;

  }

  abrirMenuLateral(){    
    if(this.abierto == true){
      this.abierto = false;
    }else{
        this.abierto = true;
    }
  }
  getParametersURL(){
    this._activatedRoute.params.subscribe((params) => {
          this.tipo = params.tipo;
          console.log('-->', params);
          this.seccion = params.seccion;
          //if (this.tipo === 'lineamientos-generales'){
          //  this.tipo = 'Lineamientos generales';
          //}
          this.informacion = this.descripcion[this.tipo];
          this.informacion = this.descripcion[this.tipo];
          // console.log('informacion=', this.informacion);
          console.log('seccion=', this.seccion);
      });
  }

}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PantallasUnicomComponent } from './pantallas-unicom.component';

describe('PantallasUnicomComponent', () => {
  let component: PantallasUnicomComponent;
  let fixture: ComponentFixture<PantallasUnicomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PantallasUnicomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PantallasUnicomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnicomComponent } from './unicom.component';

describe('UnicomComponent', () => {
  let component: UnicomComponent;
  let fixture: ComponentFixture<UnicomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnicomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnicomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[Mail]'
})
export class MailDirective {

  constructor(public _el: ElementRef) {
         // this.el.nativeElement.onkeypress = (evt) => {
         //     if ( (evt.which >= 33 && evt.which <= 39 && evt.which >= 42 && evt.which <= 46 && evt.which >= 48 && evt.which <= 59 ) || evt.which == 91 || evt.which == 93 || evt.which == 95 || evt.which == 123 ||
         //           evt.which == 125 || evt.which >= 60 && evt.which <= 64 || evt.which == 124 || evt.which == 161 || evt.which == 168 ||
         //           evt.which == 176 || evt.which == 191 ) {
         //         evt.preventDefault();
         //     }
         // };
     }

      @HostListener('input', ['$event']) onInputChange(event) {
        const initalValue = this._el.nativeElement.value;
        this._el.nativeElement.value = initalValue.replace(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i, '');
        console.log(initalValue)
        console.log(this._el.nativeElement.value)
        if ( initalValue !== this._el.nativeElement.value) {
          event.stopPropagation();
        }
      }
}

import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
//configurations
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
//Componentes
import { AppComponent } from './app.component';
import { EncabezadoComponent } from './components/compartido/encabezado/encabezado.component';
import { PieDePaginaComponent } from './components/compartido/pie-de-pagina/pie-de-pagina.component';
import { PantallasUnicomComponent } from './components/pantallas-unicom/pantallas-unicom.component';
import { UnicomComponent } from './components/pantallas-unicom/unicom/unicom.component';
import { CustomDatepickerI18n, I18n } from "./config/ng-bootstrap-datepicker-i18n";
import { NgbDateParserFormatterEsMX } from "./config/ng-bootstrap.date-parser-formatter";
import { MailDirective } from './directives/mail.directive';

import { NumerosDirective } from './directives/numeros.directive';
import { OnlyTextDirective } from './directives/only-text.directive';
import { SugerenciasPipe } from './pipes/sugerencia.pipe';
import { BuscarService } from './services/buscar.service';
import { CopyToClipboardService } from './services/copiaraportapapeles.service';
import { GlobalService } from './services/global.service';
//Servicios
import { MenuService } from './services/menu.service';
import { ThemeModule } from './theme/theme.module';

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    PieDePaginaComponent,
    OnlyTextDirective,
    NumerosDirective,
    MailDirective,
    PantallasUnicomComponent,
    UnicomComponent,
    SugerenciasPipe,
    
    
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserModule,
    FormsModule,
    ThemeModule
  ],
  providers: [ MenuService,
    BuscarService,
    GlobalService,
  CopyToClipboardService,
  I18n,
          {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
          {provide: NgbDateParserFormatter, useClass: NgbDateParserFormatterEsMX} ],
  bootstrap: [AppComponent]
})
export class AppModule { }

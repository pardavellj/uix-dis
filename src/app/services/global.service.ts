import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

wsMenuLateral = 0;

  constructor() {
  }

  gotoSection(nSeccion) {
    const el = document.getElementById(`${nSeccion}`).offsetTop + 253;
    window.scroll(0, el);
  }
}

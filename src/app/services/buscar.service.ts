import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators"

@Injectable()
export class BuscarService {
    constructor(private http: HttpClient){}

    getBuscar(): Observable<any> {
        return this.http.get<{data: any}>('assets/json/metadatos-busqueda.json')
            .pipe(map( res => res.data));
    }
}

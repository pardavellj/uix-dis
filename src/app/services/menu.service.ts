import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import 'rxjs/add/operator/map';

@Injectable()

export class MenuService {
  Url = "assets/json/menu.json"
  constructor(private _http:HttpClient) {
  }
  getmenu(){
    return this._http.get(this.Url)
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { DatosBusquedaModel } from '../interfaces/busqueda';
@Pipe({
  name: 'sugerenciasFilter',
  pure: false
})
export class SugerenciasPipe implements PipeTransform {
  // transform(items: any[], filtrar: DatosBusquedaModel[]): DatosBusquedaModel[] {
  //   if (!items || !filtrar) {
  //     return items;
  //   }
  //   return items.filter(item => item.buscar.indexOf(filtrar) !== -1);
  // }

  // applyFilter(datosBusqueda: DatosBusquedaModel, filtrar: DatosBusquedaModel): boolean {
  //   for (let field in filtrar) {
  //     if (filtrar[field]) {
  //       if (typeof filtrar[field] === 'string') {
  //         if (datosBusqueda[field].toLowerCase().indexOf(filtrar[field].toLowerCase()) === -1) {
  //           return false;
  //         }
  //       } else if (typeof filtrar[field] === 'number') {
  //         if (datosBusqueda[field] !== filtrar[field]) {
  //           return false;
  //         }
  //       }
  //     }
  //   }
  //   return true;
  // }

  transform(items: any[], filtrar: DatosBusquedaModel[]): DatosBusquedaModel[] {
    if (!items || !filtrar) {
      return items;
    }
    return items.filter(item => item.buscar.indexOf(filtrar) !== -1);
  }
  applyFilter(datosBusqueda: DatosBusquedaModel, filtrar: DatosBusquedaModel): boolean {
    for (let field in filtrar) {
      if (filtrar[field]) {
        if (typeof filtrar[field] === 'string') {
          if (datosBusqueda[field].toLowerCase().indexOf(filtrar[field].toLowerCase()) === -1) {
            return false;
          }
        } else if (typeof filtrar[field] === 'number') {
          if (datosBusqueda[field] !== filtrar[field]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}

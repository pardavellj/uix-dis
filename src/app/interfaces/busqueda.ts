export interface DatosBusquedaModel {
    id: number;
    buscar: String;
    titulo: String;
    texto: String;
    enlace: String;
    sugerencia: String;
    imagen: String
}
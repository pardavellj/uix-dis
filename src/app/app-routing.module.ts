import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: 'inicio',
    loadChildren: './components/inicio/inicio.module#InicioModule'
  },
  {
    path: 'buscador/:textobuscado',
    loadChildren: './components/buscador/buscador.module#BuscadorModule'
  },
  {
    path: 'experiencia-de-usuario/:general/:tipo/:seccion',
    loadChildren: './components/componentes-ux/componentes-ux.module#ComponentesUXModule'
  },
  {
    path: 'componentes-ui/:general/:tipo/:seccion',
    loadChildren: './components/componentes-ui/componentes-ui.module#ComponentesUIModule'
  },
  {
    path: 'documentacion-de-desarrollo/:general/:tipo/:seccion',
    loadChildren: './components/documentacion-de-desarrollo/documentacion-de-desarrollo.module#DocumentacionDeDesarrolloModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})